package main

import (
	"fmt"
	"gitlab.com/stpatrick876/nummanip/calc"
)

func main() {
	result, err := calc.Add(1, 9)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("sum: ", result)
	}
}
