package calc

import "errors"

// Add  adds provided numbers
func Add(numbers ...int) (int, error) {
	var sum int
	if len(numbers) < 2 {
		return sum, errors.New("Please provide 2 or more numbers")
	}
	for _, num := range numbers {
		sum = sum + num
	}
	return sum, nil
}
